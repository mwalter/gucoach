/*
 * GUCoach - your personal coach for Goalunited (tm).
 * Licenced under General Public Licence v3 (GPLv3)
 * newInstance.org, 2012
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.newinstance.gucoach.utility;

/**
 * Contains all named queries.
 *
 * @author mwalter
 */
public enum NamedQuery {
    FIND_ALL_FIXTURE,
    FIND_ALL_IMPORT_DATE,
    FIND_ALL_PLAYER,
    FIND_ALL_TEAM,
    FIND_LATEST_IMPORT_DATE,
    FIND_PLAYER_HISTORY_BY_PLAYER,
    FIND_PLAYER_STATS_BY_PLAYER,
    FIND_STANDINGS_HISTORY_BY_TEAM_AND_DATE,
    REMOVE_ALL_FIXTURE,
    REMOVE_ALL_STANDINGS_HISTORY,
    REMOVE_ALL_TEAM
}
